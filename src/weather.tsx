import type {
	CurrentWeather,
	OpenWeatherAPI as OpenWeatherAPIType,
} from 'openweather-api-node';
const OpenWeatherAPI = require('openweather-api-node')
	.default as typeof OpenWeatherAPIType;

import { formatHoursMins } from './index';

const api = new OpenWeatherAPI({
	key: Bun.env.OPENWEATHER_KEY,
	coordinates: {
		lat: parseFloat(Bun.env.OPENWEATHER_LAT ?? '0'),
		lon: parseFloat(Bun.env.OPENWEATHER_LON ?? '0'),
	},
	units: 'imperial',
});

let cached: CurrentWeather = await api.getCurrent();
let cachedAt = new Date().getTime();

const MAX_AGE = 2 * 60 * 1000;

const getWeather = () => {
	// trigger refresh without blocking
	if (new Date().getTime() - cachedAt >= MAX_AGE) {
		api.getCurrent().then((updated) => {
			cached = updated;
			cachedAt = new Date().getTime();
		});

		return [cached, true] as const;
	} else {
		return [cached, false] as const;
	}
};

const Wind = () => {
	const [weather] = getWeather();

	return (
		<div>
			<img height="16px" src="/wind.svg"></img>{' '}
			{weather.weather.wind.speed}mph
		</div>
	);
};

const Sunrise = () => {
	const [weather] = getWeather();

	let nextSunrise = weather.astronomical.sunriseRaw * 1000;
	let nextSunset = weather.astronomical.sunsetRaw * 1000;
	const now = new Date().getTime();
	if (nextSunrise < now) {
		nextSunrise += 24 * 60 * 60 * 1000;
	}
	if (nextSunset < now) {
		nextSunset += 24 * 60 * 60 * 1000;
	}

	if (nextSunrise < nextSunset) {
		const hours = new Date(nextSunrise).getHours();
		const mins = new Date(nextSunrise).getMinutes();

		return (
			<div>
				<img height="16px" src="/sunrise.svg"></img> sunrise at{' '}
				{formatHoursMins(hours, mins)}
			</div>
		);
	} else {
		const hours = new Date(nextSunset).getHours();
		const mins = new Date(nextSunset).getMinutes();

		return (
			<div>
				<img height="16px" src="/sunset.svg"></img> sunset at{' '}
				{formatHoursMins(hours, mins)}
			</div>
		);
	}
};

const ICONS: { [k: number]: string } = {
	200: 'cloud-lightning',
	201: 'cloud-lightning',
	202: 'cloud-lightning',
	210: 'cloud-lightning',
	211: 'cloud-lightning',
	212: 'cloud-lightning',
	221: 'cloud-lightning',
	230: 'cloud-lightning',
	231: 'cloud-lightning',
	232: 'cloud-lightning',
	300: 'cloud-drizzle',
	301: 'cloud-drizzle',
	302: 'cloud-drizzle',
	310: 'cloud-drizzle',
	311: 'cloud-drizzle',
	312: 'cloud-drizzle',
	313: 'cloud-drizzle',
	314: 'cloud-drizzle',
	321: 'cloud-drizzle',
	500: 'cloud-rain',
	501: 'cloud-rain',
	502: 'cloud-rain',
	503: 'cloud-rain',
	504: 'cloud-rain',
	511: 'cloud-snow',
	520: 'cloud-rain',
	521: 'cloud-rain',
	522: 'cloud-rain',
	531: 'cloud-rain',
	600: 'cloud-snow',
	601: 'cloud-snow',
	602: 'cloud-snow',
	611: 'cloud-snow',
	612: 'cloud-snow',
	613: 'cloud-snow',
	615: 'cloud-snow',
	616: 'cloud-snow',
	620: 'cloud-snow',
	621: 'cloud-snow',
	622: 'cloud-snow',
	701: 'cloud-fog',
	711: 'cloud-fog',
	721: 'cloud-fog',
	731: 'cloud-fog',
	741: 'cloud-fog',
	751: 'cloud-fog',
	761: 'cloud-fog',
	762: 'cloud-fog',
	771: 'cloud-fog',
	781: 'cloud-fog',
	800: 'sun',
	801: 'cloud-sun',
	802: 'cloud',
	803: 'cloudy',
	804: 'cloudy',
};

const fToC = (f: number) => ((f - 32) * 5) / 9;

export const Weather = () => {
	const [weather, isStale] = getWeather();

	return (
		<div
			className="weather"
			hx-get="/partials/weather"
			hx-swap="outerHTML"
			hx-trigger={isStale ? 'load delay:1s, every 120s' : 'every 120s'}
		>
			<h1>
				{isStale ? (
					<img src={`/loader.svg`}></img>
				) : (
					<img
						src={`/${ICONS[weather.weather.conditionId]}.svg`}
					></img>
				)}
				{weather.weather.temp.cur.toFixed(1)}&#176;F
				<span className="weather-separator">-</span>
				<br className="mobile-only" />
				{weather.weather.description}
			</h1>

			{/* <h4>{fToC(weather.weather.temp.cur).toFixed(2)}&#176;C</h4> */}
			{/* {JSON.stringify(weather)} */}

			<Wind />
			<Sunrise />
		</div>
	);
};
