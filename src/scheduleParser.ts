interface Course {
	crn: number;
	course: string;
	title: string;
	campus: string;
	credits: number;
	mode: string;
	startDate: string;
	endDate: string;
	days: string;
	time: [number, number];
	timeString: [string, string];
	timeRaw: string;
	location: string;
	instructor: string;

	courseAndSection: string;
}

const parseTimeRange = (t: string): [number, number] => {
	const [start, end] = t.split(' - ').map(parse24hTime);
	return [start, end];
};

const parse24hTime = (t: string) => {
	const [left, right] = t.split(':').map((x) => x.trim());
	const min = right.substring(0, 2);
	const pmOffset = left != '12' && right.endsWith('pm') ? 12 : 0;
	return parseInt(left) + parseInt(min) / 60 + pmOffset;
};

export const COURSES: Course[] = (await Bun.file('schedule.txt').text())
	.trim()
	.split('\n')
	.map((str) => {
		const parts = str.split('\t');

		let courseAndSection = parts[1];
		if (parts[2].startsWith('+')) {
			courseAndSection += ' (' + parts[2].replace('+', '').trim() + ')';
		}

		const [timeStartString, timeEndString] = parts[9].split(' - ');

		return {
			crn: parseInt(parts[0]),
			course: parts[1],
			title: parts[2],
			campus: parts[3],
			credits: parseFloat(parts[4]),
			mode: parts[5],
			startDate: parts[6],
			endDate: parts[7],
			days: parts[8],

			time: parseTimeRange(parts[9]),
			timeString: [timeStartString, timeEndString],
			timeRaw: parts[9],

			location: parts[10],
			instructor: parts[11],

			courseAndSection,
		} satisfies Course;
	});
