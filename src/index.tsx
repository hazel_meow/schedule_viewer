import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import { createContext } from 'preact';
import preactRender from 'preact-render-to-string/jsx';
import { useContext } from 'preact/hooks';
import { COURSES } from './scheduleParser';
import { Weather } from './weather';

const AuthContext = createContext(false);
const useAuth = () => {
	return useContext(AuthContext);
};

const Providers: preact.FunctionComponent<{
	children: preact.VNode;
	isAuthed: boolean;
}> = ({ children, isAuthed }) => (
	<AuthContext.Provider value={isAuthed}>{children}</AuthContext.Provider>
);

const renderPage = (
	Page: preact.ComponentType,
	title: string,
	isAuthed: boolean = false
): string => {
	return `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>${title}</title>
	<script src="/htmx.min.js"></script>
	<link rel="stylesheet" href="/styles.css">
	<link rel="manifest" href="/schedule.webmanifest">
</head>
<body>
${preactRender(
	<Providers isAuthed={isAuthed}>
		<Page />
	</Providers>,
	{},
	{ pretty: true }
)}
</body>`;
};
const renderPartial = (
	Partial: preact.ComponentType,
	isAuthed: boolean = false
): string => {
	return preactRender(
		<Providers isAuthed={isAuthed}>
			<Partial />
		</Providers>,
		{},
		{ pretty: true }
	);
};

const NBSP = '\u00A0';

const DAYS = ['U', 'M', 'T', 'W', 'R', 'F', 'S'];
const DAY_LABELS: { [k: (typeof DAYS)[number]]: string } = {
	U: 'sunday',
	M: 'monday',
	T: 'tuesday',
	W: 'wednesday',
	R: 'thursday',
	F: 'friday',
	S: 'saturday',
};

const CoursesForDay = ({ day }: { day: string }) => {
	const isToday = DAYS[new Date().getDay()] == day;

	let courses = COURSES.filter((c) => c.days.includes(day));

	const now = new Date().getHours() + new Date().getMinutes() / 60;

	const firstHour = Math.floor(Math.min(...COURSES.map((c) => c.time[0])));
	const lastHour = Math.ceil(Math.max(...COURSES.map((c) => c.time[1])));
	let hours = [];
	for (let h = firstHour; h <= lastHour; h++) {
		hours.push(h);
	}

	const htmxAttrs = isToday
		? {
				'hx-get': '/partials/today',
				'hx-trigger': 'every 60s',
				'hx-swap': 'outerHTML',
		  }
		: {};

	const auth = useAuth();

	return (
		<div className="schedule" {...htmxAttrs}>
			<h3 className="weekday-label">{DAY_LABELS[day]}</h3>

			{hours.map((hour) => (
				<div className="hour">
					<span>
						{(
							(hour > 12 ? hour - 12 : hour) +
							(hour >= 12 ? 'p' : 'a')
						)
							.padStart(3, 'X')
							.replace('X', NBSP)}
					</span>
					<div className="tick" />
					{hour != lastHour && <div className="tick half-past" />}
				</div>
			))}

			{courses.map((c) => {
				const topOffset = (c.time[0] - firstHour) * 64;
				const height = (c.time[1] - c.time[0]) * 64;

				return (
					<div
						className="course"
						style={`top: ${topOffset}px; height: ${height}px;`}
					>
						<div>
							{auth ? c.courseAndSection : c.course.split(' ')[0]}
						</div>
						<div>{c.timeRaw}</div>
						{auth && <div>{c.location}</div>}
					</div>
				);
			})}

			{isToday && now >= firstHour && now <= lastHour && (
				<NowTick firstHour={firstHour} />
			)}
		</div>
	);
};

const NowTick = ({ firstHour }: { firstHour: number }) => {
	const now = new Date().getHours() + new Date().getMinutes() / 60;
	const topOffset = (now - firstHour) * 64;

	return <div className="now" style={`top: ${topOffset}px`}></div>;
};

const CurrentNext = () => {
	const todayLetter = DAYS[new Date().getDay()];
	const todayCourses = COURSES.filter((c) =>
		c.days.includes(todayLetter)
	).sort((a, b) => a.time[0] - b.time[0]);

	const now = new Date().getHours() + new Date().getMinutes() / 60;

	const currentCourse = todayCourses.find(
		(c) => c.time[0] <= now && c.time[1] >= now
	);
	const nextCourse = todayCourses.find(
		(c) => c != currentCourse && c.time[0] >= now
	);

	const auth = useAuth();

	return (
		<div hx-get="/partials/next" hx-swap="outerHTML" hx-trigger="every 60s">
			{currentCourse ? (
				<h2>
					current: {auth && currentCourse.courseAndSection} until{' '}
					{currentCourse.timeString[1]}
				</h2>
			) : (
				<h2>-</h2>
			)}
			{nextCourse ? (
				<h2>
					next: {auth && nextCourse.courseAndSection} at{' '}
					{nextCourse.timeString[0]}
				</h2>
			) : (
				<h2>-</h2>
			)}
		</div>
	);
};

// !!! nested ternary alert !!!
// !!! code-prettier-makes-uglier alert !!!
export const formatHoursMins = (hours: number, mins: number) =>
	`${hours == 0 ? 12 : hours > 12 ? hours - 12 : hours}:${mins
		.toString()
		.padStart(2, '0')}${hours >= 12 ? 'p' : 'a'}`;

const Clock = () => {
	const hours = new Date().getHours();
	const mins = new Date().getMinutes();
	const day = new Date().getDay();
	const month = new Date().getMonth();
	const date = new Date().getDate();

	const startDate = new Date(COURSES[0].startDate);
	const week =
		(new Date().getTime() - startDate.getTime()) /
		(1000 * 60 * 60 * 24 * 7);

	return (
		<div
			hx-get="/partials/clock"
			hx-swap="outerHTML"
			hx-trigger="every 60s"
			className="date"
		>
			<h1>{formatHoursMins(hours, mins)}</h1>

			<div>
				<img height="16px" src="/calendar.svg"></img>{' '}
				{
					[
						'sunday',
						'monday',
						'tuesday',
						'wednesday',
						'thursday',
						'friday',
						'saturday',
					][day]
				}
				,{' '}
				{
					[
						'jan',
						'feb',
						'mar',
						'apr',
						'may',
						'jun',
						'jul',
						'aug',
						'sept',
						'oct',
						'nov',
						'dec',
					][month]
				}{' '}
				{date}
			</div>

			<div>
				<img height="16px" src="/calendar-range.svg"></img> week{' '}
				{Math.ceil(week)}
			</div>
		</div>
	);
};

const TodayPartial = () => {
	const today = DAYS[new Date().getDay()];
	return <CoursesForDay day={today} />;
};

const TodayPage = () => {
	const isAuth = useAuth();

	return (
		<main>
			<div className="details">
				<Clock />

				<Weather />

				<div>
					<CurrentNext />

					<div>
						<a href="/full">full schedule</a>
						{!isAuth && (
							<>
								| <a href="/auth">authorize</a>
							</>
						)}
					</div>
				</div>
			</div>

			{TodayPartial()}
		</main>
	);
};

const FullSchedulePage = () => {
	return (
		<main className="full-schedule">
			<a className="mobile-only" href="/">
				back to today
			</a>

			<CoursesForDay day="M" />
			<CoursesForDay day="T" />
			<CoursesForDay day="W" />
			<CoursesForDay day="R" />
			<CoursesForDay day="F" />
		</main>
	);
};

const AuthPage = () => {
	return (
		<main>
			<a className="mobile-only" href="/">
				back to today
			</a>

			<form method="POST">
				<label for="password">secret:</label>
				<input id="password" name="password" type="password"></input>
				<input type="submit"></input>
			</form>
		</main>
	);
};

const app = express();

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));

declare global {
	namespace Express {
		interface Request {
			isAuthed?: boolean;
		}
	}
}

app.use('/', (req, res, next) => {
	req.isAuthed = req.cookies.auth == process.env.APP_SECRET;
	next();
});

app.use('/', express.static('public'));

app.get('/', (req, res) => {
	res.send(renderPage(TodayPage, 'today', req.isAuthed));
});
app.get('/full', (req, res) => {
	res.send(renderPage(FullSchedulePage, 'full schedule', req.isAuthed));
});
app.get('/auth', (req, res) => {
	if (req.isAuthed) {
		return res.redirect('/');
	}
	res.send(renderPage(AuthPage, 'authorize', req.isAuthed));
});
app.post('/auth', (req, res) => {
	res.cookie('auth', req.body.password, {
		maxAge: 365 * 24 * 60 * 60 * 1000,
	});
	res.redirect('/auth');
});
app.get('/partials/today', (req, res) => {
	res.send(renderPartial(TodayPartial, req.isAuthed));
});
app.get('/partials/next', (req, res) => {
	res.send(renderPartial(CurrentNext, req.isAuthed));
});
app.get('/partials/weather', (req, res) => {
	res.send(renderPartial(Weather, req.isAuthed));
});
app.get('/partials/clock', (req, res) => {
	res.send(renderPartial(Clock, req.isAuthed));
});

const port = parseInt(Bun.env.PORT ?? '3000');
app.listen({
	port,
});

console.log(`server running on port ${port}`);
